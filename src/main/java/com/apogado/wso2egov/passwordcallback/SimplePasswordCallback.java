package com.apogado.wso2egov.passwordcallback;
 
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.log4j.Logger;
import org.apache.ws.security.WSPasswordCallback;
 
public class SimplePasswordCallback
  implements CallbackHandler
{
  private static final Logger logger = Logger.getLogger(SimplePasswordCallback.class);
  private static final String CONF_FILE = "./repository/conf/ws-sec-credentials.properties";
  private Properties userCredentials;
 
  public SimplePasswordCallback()
  {
    this.userCredentials = new Properties();
    try
    {
      readCredentialProperties();
    }
    catch (Exception ex)
    {
      logger.error("reading config file", ex);
    }
  }
 
  public void handle(Callback[] callbacks)
    throws IOException, UnsupportedCallbackException
  {
    if (callbacks == null) {
      return;
    }
    for (Callback callback : callbacks) {
      if (!(callback instanceof WSPasswordCallback))
      {
        logger.warn("Unexpected callback type: " + String.valueOf(callback));
      }
      else
      {
        WSPasswordCallback pwcb = (WSPasswordCallback)callback;
        String id = pwcb.getIdentifier();
        int usage = pwcb.getUsage();
        if ((id != null) && (this.userCredentials.containsKey(id)))
        {
          if (logger.isDebugEnabled()) {
            logger.debug("Providing password callback for user: " + id);
          }
          pwcb.setPassword(this.userCredentials.getProperty(id));
        }
        else
        {
          logger.warn("Requested credentials for unknown user: " + id);
        }
      }
    }
  }
 
  private void readCredentialProperties()
    throws IOException
  {
    File f = new File(CONF_FILE);
    if (!f.exists())
    {
      logger.error("Data file not found: " + f.getAbsolutePath());
      return;
    }
    Reader in = new InputStreamReader(new FileInputStream(f), "UTF-8");
    this.userCredentials.load(in);
    in.close();
  }
}