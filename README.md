# README #

Implementation of the password callback handler used for WSO2ESB WS-Security features (signing). 


### Environment assumptions ###

 - There must be a keystore with a signing / encryption keypair
 - Alias name of the keypair must not contains spaces

### Installation and setup ###

 - Copy the binary jar file into the *$WSO2ESB/repository/components/lib*
 - create a property file *./repository/conf/ws-sec-credentials.properties* with records

> username=password

where the username must match the alias in the keystore (therefore do not use spaces for the keystore aliases)

### Example policy ###

Example WS-Policy files are stored in the test resources (src/test/resources/sample/policies/)

